<?php

namespace App\Service;

use Exception;
use GuzzleHttp\Client;

class YoutubeService
{
    const API_KEY = 'AIzaSyDAkMFP-pQ90F5VkrgzHhkoLqrbN7Q_RD0';
    const API_BASE_URL = 'https://www.googleapis.com/youtube/v3/search';
    const API_RESULTS_PER_PAGE = 10;

    /**
     * @var Client
     */
    private $client;

    public function __construct()
    {
        $this->client = new Client();
    }

    /**
     * @param string      $searchQuery
     * @param string|null $pageToken
     *
     * @return object|null
     */
    public function fetchSearchResults(string $searchQuery, ?string $pageToken = null): ?object
    {
        $queryData = [
            'part' => 'snippet',
            'type' => 'video',
            'key' => self::API_KEY,
            'q' => $searchQuery,
            'maxResults' => self::API_RESULTS_PER_PAGE
        ];

        if (!empty($pageToken)) {
            $queryData['pageToken'] = $pageToken;
        }

        $youtubeQueryUrl = sprintf('%s?%s', self::API_BASE_URL, http_build_query($queryData));

        try {
            $results = json_decode($this->client->get($youtubeQueryUrl)
                ->getBody()
                ->getContents());
        } catch (Exception $exception) {
            return null;
        }

        return $results;
    }
}