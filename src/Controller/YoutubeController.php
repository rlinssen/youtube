<?php

namespace App\Controller;

use App\Form\YoutubeSearchFormType;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;

class YoutubeController extends AbstractController
{
	/**
     * @Route("/", name="youtube_index")
     * @param Request $request
     *
     * @return Response
     */
    public function indexAction(Request $request): Response
    {
		$searchForm = $this->createForm(YoutubeSearchFormType::class);
		$searchForm->handleRequest($request);

		return $this->render('search.html.twig', [
			'form' => $searchForm->createView()
		]);
	}
}