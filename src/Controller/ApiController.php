<?php

namespace App\Controller;

use App\Service\YoutubeService;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;

class ApiController extends AbstractController
{
    /**
     * @var YoutubeService
     */
    private $youtubeService;

    /**
     * ApiController constructor.
     *
     * @param YoutubeService $youtubeService
     */
    public function __construct(
        YoutubeService $youtubeService
    ) {
        $this->youtubeService = $youtubeService;
    }

    /**
     * @Route("/results/{searchQuery}/{pageToken}", name="youtube_api_results")
     * @param string      $searchQuery
     * @param string|null $pageToken
     *
     * @return Response
     */
    public function resultsAction(string $searchQuery, ?string $pageToken = null): Response
    {
        if (empty($searchQuery)) {
            return $this->redirectToRoute('youtube_index');
        }

        $searchResults = $this->youtubeService->fetchSearchResults($searchQuery, $pageToken);

        return $this->render('searchresults.html.twig', [
            'searchResults' => $searchResults,
            'searchQuery' => $searchQuery,
        ]);
    }
}