$(function () {

    $("#youtube_search_form_searchWords").keypress(function(e) {
        if (e.keyCode == 13) {
            e.preventDefault();
            $("#youtubeSearch").click();
        }
    });

    $("#youtubeSearch").bind("click", function() {
        var searchQuery = $("#youtube_search_form_searchWords").val();

        if (searchQuery != '') {
            doSearch(searchQuery, null);
        }
    });

    function paginationHandler(token) {
        var searchQuery = $("#youtube_search_form_searchWords").val();

        if (searchQuery != '') {
            doSearch(searchQuery, token);
        }
    }

    function doSearch(searchQuery, pageToken) {
        $("#searchResults").html('');
        $("#spinner").show();
        var queryUrl = '/results/' + searchQuery;

        if (pageToken != null) {
            queryUrl = queryUrl + '/' + pageToken;
        }
        $.ajax({
            url: queryUrl,
            method: 'GET'
        }).done(function (rawhtml) {
            $("#spinner").hide();
            $("#searchResults").html(rawhtml);

            $(".youtubeNextPage").bind("click", function() {
                var token = $(this).data('next-page-token');
                paginationHandler(token);
            });

            $(".youtubePreviousPage").bind("click", function() {
                var token = $(this).data('prev-page-token');
                paginationHandler(token);
            });
        });
    }

});